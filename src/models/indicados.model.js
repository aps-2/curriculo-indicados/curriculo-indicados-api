// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient')
  const indicados = sequelizeClient.define(
    'indicados',
    {
      nome: {
        type: DataTypes.STRING,
        allowNull: false
      },
      status: {
        type: DataTypes.ENUM('alternativa', 'indicado'),
        defaultValue: 'alternativa'
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true
        }
      }
    }
  )

  // eslint-disable-next-line no-unused-vars
  indicados.associate = function(models) {
    const { cargos, indicados } = models
    indicados.belongsTo(cargos, {
      foreignKey: {
        allowNull: false
      }
    })
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return indicados
}
