// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient')
  const locals = sequelizeClient.define(
    'locals',
    {
      nome: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true
        }
      }
    }
  )

  // eslint-disable-next-line no-unused-vars
  locals.associate = function(models) {
    const { locals, cargos } = models
    locals.hasMany(cargos)
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return locals
}
