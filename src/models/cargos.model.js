// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize')
const DataTypes = Sequelize.DataTypes

module.exports = function(app) {
  const sequelizeClient = app.get('sequelizeClient')
  const cargos = sequelizeClient.define(
    'cargos',
    {
      nome: {
        type: DataTypes.STRING,
        allowNull: false
      }
    },
    {
      hooks: {
        beforeCount(options) {
          options.raw = true
        }
      }
    }
  )

  // eslint-disable-next-line no-unused-vars
  cargos.associate = function(models) {
    const { cargos, locals } = models
    cargos.belongsTo(locals, {
      foreignKey: {
        allowNull: false
      }
    })
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  }

  return cargos
}
