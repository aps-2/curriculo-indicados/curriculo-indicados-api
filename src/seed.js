const seed = async app => {
  try {
    await app.setup()
    await app.get('sequelizeSync')

    // users
    users.forEach(user => app.service('users').create(user))
    locals.forEach(local => app.service('locals').create(local))
    cargos.forEach(cargo => app.service('cargos').create(cargo))

    // Adicione outros abaixo
  } catch (error) {
    console.error(error)
  }
}

module.exports = seed

// Usuários padrões
const users = [
  {
    name: 'Administrador',
    email: 'admin@admin.com',
    role: 'admin',
    password: '123'
  },
  {
    name: 'Moderador',
    email: 'mod@mod.com',
    role: 'mod',
    password: '123'
  },
  {
    name: 'Usuário',
    email: 'user@user.com',
    role: 'user',
    password: '123'
  }
]

const locals = [
  {
    nome: 'TJ'
  },
  {
    nome: 'TCE'
  },
  {
    nome: 'TRE'
  }
]

const cargos = [
  {
    nome: 'Coordenador',
    localId: 1
  },
  {
    nome: 'Gerente',
    localId: 1
  },
  {
    nome: 'Superintendente',
    localId: 2
  }
]
