const { authenticate } = require('@feathersjs/authentication').hooks
const useIncludeQuery = require('../../hooks/use-include-query')
const useLikeQuery = require('../../hooks/use-like-query')
const useSortQuery = require('../../hooks/use-sort-query')

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [useIncludeQuery(), useSortQuery(), useLikeQuery()],
    get: [useIncludeQuery()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
