const users = require('./users/users.service.js')
const cargos = require('./cargos/cargos.service.js')
const locals = require('./locals/locals.service.js')
const indicados = require('./indicados/indicados.service.js')
// eslint-disable-next-line no-unused-vars
module.exports = function(app) {
  app.configure(users)
  app.configure(cargos)
  app.configure(locals)
  app.configure(indicados)
}
