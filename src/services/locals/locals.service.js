// Initializes the `locals` service on path `/locals`
const { Locals } = require('./locals.class')
const createModel = require('../../models/locals.model')
const hooks = require('./locals.hooks')

module.exports = function(app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  }

  // Initialize our service with any options it requires
  app.use('/locals', new Locals(options, app))

  // Get our initialized service so that we can register hooks
  const service = app.service('locals')

  service.hooks(hooks)
}
