const { authenticate } = require('@feathersjs/authentication').hooks
const useIncludeQuery = require('../../hooks/use-include-query')
const useLikeQuery = require('../../hooks/use-like-query')
const useSortQuery = require('../../hooks/use-sort-query')

module.exports = {
  before: {
    all: [],
    find: [useIncludeQuery(), useSortQuery(), useLikeQuery()],
    get: [useIncludeQuery()],
    create: [authenticate('jwt')],
    update: [authenticate('jwt')],
    patch: [authenticate('jwt')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
