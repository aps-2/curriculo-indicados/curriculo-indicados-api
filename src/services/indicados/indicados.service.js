// Initializes the `indicados` service on path `/indicados`
const { Indicados } = require('./indicados.class');
const createModel = require('../../models/indicados.model');
const hooks = require('./indicados.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/indicados', new Indicados(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('indicados');

  service.hooks(hooks);
};
