const { authenticate } = require('@feathersjs/authentication').hooks
const {
  hashPassword,
  protect
} = require('@feathersjs/authentication-local').hooks
const useIncludeQuery = require('../../hooks/use-include-query')
const useLikeQuery = require('../../hooks/use-like-query')
const useSortQuery = require('../../hooks/use-sort-query')

module.exports = {
  before: {
    all: [],
    find: [
      authenticate('jwt'),
      useIncludeQuery(),
      useSortQuery(),
      useLikeQuery()
    ],
    get: [authenticate('jwt'), useIncludeQuery()],
    create: [hashPassword('password')],
    update: [authenticate('jwt'), hashPassword('password')],
    patch: [authenticate('jwt'), hashPassword('password')],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password field is never sent to the client
      // Always must be the last hook
      protect('password')
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
