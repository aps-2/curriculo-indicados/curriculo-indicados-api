const {
  AuthenticationService,
  JWTStrategy
} = require('@feathersjs/authentication')
const { LocalStrategy } = require('@feathersjs/authentication-local')
const { expressOauth } = require('@feathersjs/authentication-oauth')

module.exports = app => {
  const commonAuth = new AuthenticationService(app)
  const myAuthentication = new AuthenticationService(app)
  myAuthentication.getPayload = async (authResult, params) => {
    const payload = await commonAuth.getPayload(authResult, params)
    const { user } = authResult

    if (user) {
      payload.name = user.name
      payload.role = user.role
      payload.email = user.email
      payload.avatar = user.avatar
    }

    return payload
  }

  myAuthentication.register('jwt', new JWTStrategy())
  myAuthentication.register('local', new LocalStrategy())

  app.use('/auth', myAuthentication)
  app.configure(expressOauth())
}
